<?php

namespace Drupal\iots_widget\Service;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\iots_widget\Entity\IotsWidget;

/**
 * IotsWidget service with settings.
 */
class WidgetData {
  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Creates a new WidgetData Service.
   */
  public function __construct() {

  }

  /**
   * GetData.
   */
  public function data(IotsWidget $widget, string | NULL $root = NULL) : array {
    $key = $widget->key->value;
    $type = $widget->type->value;
    if ($root) {

    }
    $result = [
      'uuid' => $widget->uuid->value,
      'name' => $widget->name->value,
      'bundle' => $widget->bundle(),
      'key' => $widget->key->value,
      'mode' => $widget->mode->value,
      'type' => $widget->type->value,
      'icon' => $this->icon($widget),
      'color' => $widget->color->value,
      'use_root' => $widget->use_root->value,
      'publish' => $widget->publish->value,
      'subscribe' => $widget->subscribe->value,
    ];
    if (empty($result['publish'])) {
      $result['publish'] = "commands/$key";
    }
    if (empty($result['subscribe'])) {
      $result['subscribe'] = "events/$key";
      if ($widget->mode->value == 'state') {
        $result['subscribe'] = "state/$key";
      }
    }
    foreach ($widget as $key => $field) {
      if (substr($key, 0, 6) == 'field_') {
        $k = substr($key, 6);
        $result['extra'][$k] = $widget->get($key)->value;
      }
    }
    return $result;
  }

  /**
   * Random icon.
   */
  public function icon(IotsWidget $widget) : string {
    $icon = $widget->icon->value;
    return $icon;
  }

}
