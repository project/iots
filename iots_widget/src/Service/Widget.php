<?php

namespace Drupal\iots_widget\Service;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\iots_widget\Entity\IotsWidget;

/**
 * IotsWidget service with settings.
 */
class Widget {
  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Creates a new Widget Service.
   */
  public function __construct() {

  }

  /**
   * GetData.
   */
  public function data(IotsWidget $layout) : array {
    $result = [];
    return $result;
  }

}
