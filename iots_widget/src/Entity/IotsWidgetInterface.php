<?php

namespace Drupal\iots_widget\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a widget entity type.
 */
interface IotsWidgetInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the widget title.
   *
   * @return string
   *   Title of the widget.
   */
  public function getTitle();

  /**
   * Sets the widget title.
   *
   * @param string $title
   *   The widget title.
   *
   * @return \Drupal\iots_widget\IotsWidgetInterface
   *   The called widget entity.
   */
  public function setTitle($title);

  /**
   * Gets the widget creation timestamp.
   *
   * @return int
   *   Creation timestamp of the widget.
   */
  public function getCreatedTime();

  /**
   * Sets the widget creation timestamp.
   *
   * @param int $timestamp
   *   The widget creation timestamp.
   *
   * @return \Drupal\iots_widget\IotsWidgetInterface
   *   The called widget entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the widget status.
   *
   * @return bool
   *   TRUE if the widget is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the widget status.
   *
   * @param bool $status
   *   TRUE to enable this widget, FALSE to disable.
   *
   * @return \Drupal\iots_widget\IotsWidgetInterface
   *   The called widget entity.
   */
  public function setStatus($status);

}
