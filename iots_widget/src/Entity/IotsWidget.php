<?php

namespace Drupal\iots_widget\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionLogEntityTrait;
use Drupal\iots\Utility\FieldDefinition;
use Drupal\iots\Utility\ContentEntity;

/**
 * Defines the widget entity class.
 *
 * @ContentEntityType(
 *   id = "iots_widget",
 *   label = @Translation("Widget"),
 *   label_collection = @Translation("Widgets"),
 *   bundle_label = @Translation("Widget type"),
 *   handlers = {
 *     "view_builder" = "Drupal\iots_widget\Entity\IotsWidgetViewBuilder",
 *     "list_builder" = "Drupal\iots_widget\Entity\IotsWidgetListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\iots_widget\Entity\IotsWidgetAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\iots_widget\Form\IotsWidgetForm",
 *       "edit" = "Drupal\iots_widget\Form\IotsWidgetForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "iots_widget",
 *   revision_table = "iots_widget_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer widget types",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "bundle" = "bundle",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/iots/widget/add/{iots_widget_type}",
 *     "add-page" = "/iots/widget/add",
 *     "canonical" = "/iots/widget/{iots_widget}",
 *     "edit-form" = "/iots/widget/{iots_widget}/edit",
 *     "delete-form" = "/iots/widget/{iots_widget}/delete",
 *     "collection" = "/iots/widget"
 *   },
 *   bundle_entity_type = "iots_widget_type",
 *   field_ui_base_route = "entity.iots_widget_type.edit_form"
 * )
 */
class IotsWidget extends ContentEntity {

  use RevisionLogEntityTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::revisionLogBaseFieldDefinitions($entity_type);
    $fields['uuid'] = FieldDefinition::unique();
    $fields['name'] = FieldDefinition::string('Name');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom.
    $fields['use_root'] = FieldDefinition::bool('Use Root Topic');
    $fields['mode'] = FieldDefinition::list('Type', [
      'events' => 'events',
      'state' => 'state',
      'commands' => 'commands',
      'config' => 'config',
    ]);
    $fields['mode']->setDefaultValue('events');
    $fields['type'] = FieldDefinition::list('Type', [
      'switch' => 'Switch',
      'number' => 'Number',
      'list' => 'List',
      'rgb' => 'RGB',
    ]);
    /** @var \Drupal\Core\Field\BaseFieldDefinition $fields['type'] */
    $fields['type']->setDefaultValue('switch');
    $fields['key'] = FieldDefinition::string('Key');
    $fields['publish'] = FieldDefinition::string('Publish');
    $fields['subscribe'] = FieldDefinition::string('Subscribe');
    $fields['icon'] = FieldDefinition::string('Icon');
    $fields['color'] = FieldDefinition::string('Сolor');
    $fields['description'] = FieldDefinition::string('Description');
    // Info.
    $fields['yaml'] = FieldDefinition::long('Yaml');
    return $fields;
  }

}
