<?php

namespace Drupal\iots_widget\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Widget type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "iots_widget_type",
 *   label = @Translation("Widget type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\iots_widget\Form\IotsWidgetTypeForm",
 *       "edit" = "Drupal\iots_widget\Form\IotsWidgetTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\iots_widget\Entity\IotsWidgetTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer widget types",
 *   bundle_of = "iots_widget",
 *   config_prefix = "iots_widget_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/iots/config/iots_widget_types/add",
 *     "edit-form" = "/admin/iots/config/iots_widget_types/manage/{iots_widget_type}",
 *     "delete-form" = "/admin/iots/config/iots_widget_types/manage/{iots_widget_type}/delete",
 *     "collection" = "/admin/iots/config/iots_widget_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class IotsWidgetType extends ConfigEntityBundleBase {

  /**
   * The machine name of this widget type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the widget type.
   *
   * @var string
   */
  protected $label;

}
