<?php

namespace Drupal\iots_widget\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the widget entity type.
 */
class IotsWidgetAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view widget');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit widget', 'administer widget'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete widget', 'administer widget'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create widget', 'administer widget'], 'OR');
  }

}
