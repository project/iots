<?php

namespace Drupal\iots_widget\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the widget entity edit forms.
 */
class IotsWidgetForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => \Drupal::service('renderer')->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New widget %label has been created.', $message_arguments));
      $this->logger('iots_widget')->notice('Created new widget %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The widget %label has been updated.', $message_arguments));
      $this->logger('iots_widget')->notice('Updated new widget %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.iots_widget.canonical', ['iots_widget' => $entity->id()]);
  }

}
