<?php

namespace Drupal\iots_registry\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a device registry entity type.
 */
interface IotsRegistryInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the device registry name.
   *
   * @return string
   *   Title of the device registry.
   */
  public function getTitle();

  /**
   * Sets the device registry title.
   *
   * @param string $title
   *   The device registry title.
   *
   * @return \Drupal\iots_registry\Entity\IotsRegistryInterface
   *   The called device registry entity.
   */
  public function setTitle($title);

  /**
   * Gets the device registry creation timestamp.
   *
   * @return int
   *   Creation timestamp of the device registry.
   */
  public function getCreatedTime();

  /**
   * Sets the device registry creation timestamp.
   *
   * @param int $timestamp
   *   The device registry creation timestamp.
   *
   * @return \Drupal\iots_registry\Entity\IotsRegistryInterface
   *   The called device registry entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the device registry status.
   *
   * @return bool
   *   TRUE if the device registry is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the device registry status.
   *
   * @param bool $status
   *   TRUE to enable this device registry, FALSE to disable.
   *
   * @return \Drupal\iots_registry\Entity\IotsRegistryInterface
   *   The called device registry entity.
   */
  public function setStatus($status);

}
