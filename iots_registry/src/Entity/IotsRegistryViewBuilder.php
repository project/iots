<?php

namespace Drupal\iots_registry\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Base class for entity view builders.
 *
 * @ingroup entity_api
 */
class IotsRegistryViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
    $build_list = $this->viewMultiple([$entity], $view_mode, $langcode);
    $build = $build_list[0];
    $build['#pre_render'][] = [$this, 'build'];
    $uuid = $entity->uuid->value;
    if ($view_mode == 'full') {
      $build['uuid'] = ['#markup' => "<h4>ID: <small>$uuid</small></h4>"];
      if (\Drupal::hasService('iots_device')) {
        $id = $entity->id();
        $build['devices'] = \Drupal::service('iots_device')->deviceList($id);
      }
      $ids = [];
      foreach ($entity->credentials->getValue() as $credential) {
        $ids[] = $credential['target_id'];
      }
      $build['credentials'] = \Drupal::service('iots_credentials')->render($ids);
    }

    return $build;
  }

}
