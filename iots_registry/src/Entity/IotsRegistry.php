<?php

namespace Drupal\iots_registry\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\iots\Utility\FieldDefinition;
use Drupal\iots\Utility\ContentEntity;
use Drupal\Core\Field\FieldStorageDefinitionInterface as FSDI;

/**
 * Defines the device registry entity class.
 *
 * @ContentEntityType(
 *   id = "iots_registry",
 *   label = @Translation("Device Registry"),
 *   label_collection = @Translation("Device Registrys"),
 *   handlers = {
 *     "view_builder" = "Drupal\iots_registry\Entity\IotsRegistryViewBuilder",
 *     "list_builder" = "Drupal\iots_registry\Entity\IotsRegistryListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\iots_registry\Form\IotsRegistryForm",
 *       "edit" = "Drupal\iots_registry\Form\IotsRegistryForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "iots_registry",
 *   revision_table = "iots_registry_revision",
 *   show_revision_ui = FALSE,
 *   admin_permission = "administer device registry",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form"    = "/iots/registry/add",
 *     "canonical"   = "/iots/registry/{iots_registry}",
 *     "edit-form"   = "/iots/registry/{iots_registry}/edit",
 *     "delete-form" = "/iots/registry/{iots_registry}/delete",
 *     "collection"  = "/iots/registry"
 *   },
 *   field_ui_base_route = "entity.iots_registry.settings"
 * )
 */
class IotsRegistry extends ContentEntity {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['uuid'] = FieldDefinition::unique();
    $fields['name'] = FieldDefinition::string('Name');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom.
    $fields['description'] = FieldDefinition::string('Description');
    $fields['credentials'] = FieldDefinition::entity('Credentials', 'iots_credentials')
      ->setCardinality(FSDI::CARDINALITY_UNLIMITED);
    // Info.
    $fields['data'] = FieldDefinition::map('Data');
    return $fields;
  }

}
