<?php

namespace Drupal\iots_registry\Service;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * IotsDevice service with settings.
 */
class Registry {
  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Creates a new Device Service.
   */
  public function __construct() {
    $entity_type = 'iots_registry';
    $this->storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    $this->entityType = \Drupal::entityTypeManager()->getDefinition($entity_type);
    $this->dateFormatter = \Drupal::service('date.formatter');
  }

  /**
   * Device List.
   */
  public function createNew($fromValues) {
    $storageCert = \Drupal::entityTypeManager()->getStorage('iots_credentials');
    if (!empty($fromValues['credentials'])) {
      foreach ($fromValues['credentials'] as $key => $value) {
        if ($value) {
          $cert = $storageCert->create([
            'type' => $key,
          ]);
          $cert->save();
          $fromValues['credentials'][] = $cert->id();
        }
        unset($fromValues['credentials'][$key]);
      }
    }
    $entity = $this->storage->create($fromValues);
    $entity->save();
    return $entity;
  }

  /**
   * Query.
   */
  private function query($registry_id) {
    $entities = [];
    $query = $this->storage->getQuery()
      ->condition('status', 1)
      ->condition('registry', $registry_id)
      ->sort('created', 'ASC')
      ->range(0, 100);
    $ids = $query->execute();
    if (!empty($ids)) {
      foreach ($this->storage->loadMultiple($ids) as $id => $entity) {
        $entities[$id] = $entity;
      }
    }
    return $entities;
  }

}
