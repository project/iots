<?php

namespace Drupal\iots_registry\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the device registry entity edit forms.
 */
class IotsRegistryForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => \Drupal::service('renderer')->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New device registry %label has been created.', $message_arguments));
      $this->logger('iots_registry')->notice('Created new device registry %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The device registry %label has been updated.', $message_arguments));
      $this->logger('iots_registry')->notice('Updated new device registry %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.iots_registry.canonical', ['iots_registry' => $entity->id()]);
  }

}
