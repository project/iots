<?php

namespace Drupal\iots_release\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a release entity type.
 */
interface IotsReleaseInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the release title.
   *
   * @return string
   *   Title of the release.
   */
  public function getTitle();

  /**
   * Sets the release title.
   *
   * @param string $title
   *   The release title.
   *
   * @return \Drupal\iots_release\IotsReleaseInterface
   *   The called release entity.
   */
  public function setTitle($title);

  /**
   * Gets the release creation timestamp.
   *
   * @return int
   *   Creation timestamp of the release.
   */
  public function getCreatedTime();

  /**
   * Sets the release creation timestamp.
   *
   * @param int $timestamp
   *   The release creation timestamp.
   *
   * @return \Drupal\iots_release\IotsReleaseInterface
   *   The called release entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the release status.
   *
   * @return bool
   *   TRUE if the release is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the release status.
   *
   * @param bool $status
   *   TRUE to enable this release, FALSE to disable.
   *
   * @return \Drupal\iots_release\IotsReleaseInterface
   *   The called release entity.
   */
  public function setStatus($status);

}
