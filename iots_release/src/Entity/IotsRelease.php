<?php

namespace Drupal\iots_release\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\iots\Utility\FieldDefinition;
use Drupal\iots\Utility\ContentEntity;
use Drupal\Core\Field\FieldStorageDefinitionInterface as FSDI;

/**
 * Defines the release entity class.
 *
 * @ContentEntityType(
 *   id = "iots_release",
 *   label = @Translation("Release"),
 *   label_collection = @Translation("Releases"),
 *   handlers = {
 *     "view_builder" = "Drupal\iots_release\Entity\IotsReleaseViewBuilder",
 *     "list_builder" = "Drupal\iots_release\Entity\IotsReleaseListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\iots_release\Entity\IotsReleaseAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\iots_release\Form\IotsReleaseForm",
 *       "edit" = "Drupal\iots_release\Form\IotsReleaseForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "iots_release",
 *   admin_permission = "administer release",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form"    = "/iots/release/add",
 *     "canonical"   = "/iots/release/{iots_release}",
 *     "edit-form"   = "/iots/release/{iots_release}/edit",
 *     "delete-form" = "/iots/release/{iots_release}/delete",
 *     "collection"  = "/iots/release"
 *   },
 *   field_ui_base_route = "entity.iots_release.settings"
 * )
 */
class IotsRelease extends ContentEntity {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['uuid'] = FieldDefinition::unique();
    $fields['name'] = FieldDefinition::string('Name');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom.
    $fields['url'] = FieldDefinition::string('Url');
    $fields['comment'] = FieldDefinition::string('Comment');
    $fields['git_commit'] = FieldDefinition::string('Commit');
    $fields['git_tag'] = FieldDefinition::string('Tag');
    // Build.
    $fields['size'] = FieldDefinition::int('Size');
    $fields['build_date'] = FieldDefinition::date('Build date');
    $fields['md5'] = FieldDefinition::string('MD5');
    $fields['sha'] = FieldDefinition::string('SHA-1');
    $fields['sha256'] = FieldDefinition::string('SHA-256');
    // SSH.
    $fields['credentials'] = FieldDefinition::entity('Credentials', 'iots_credentials')
      ->setCardinality(FSDI::CARDINALITY_UNLIMITED);
    // Info.
    $fields['yaml'] = FieldDefinition::long('Yaml');
    return $fields;
  }

}
