<?php

namespace Drupal\iots_release\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the release entity edit forms.
 */
class IotsReleaseForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => \Drupal::service('renderer')->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New release %label has been created.', $message_arguments));
      $this->logger('iots_release')->notice('Created new release %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The release %label has been updated.', $message_arguments));
      $this->logger('iots_release')->notice('Updated new release %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.iots_release.canonical', ['iots_release' => $entity->id()]);
  }

}
