<?php

namespace Drupal\iots_deployment;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a deployment entity type.
 */
interface IotsDeploymentInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the deployment title.
   *
   * @return string
   *   Title of the deployment.
   */
  public function getTitle();

  /**
   * Sets the deployment title.
   *
   * @param string $title
   *   The deployment title.
   *
   * @return \Drupal\iots_deployment\IotsDeploymentInterface
   *   The called deployment entity.
   */
  public function setTitle($title);

  /**
   * Gets the deployment creation timestamp.
   *
   * @return int
   *   Creation timestamp of the deployment.
   */
  public function getCreatedTime();

  /**
   * Sets the deployment creation timestamp.
   *
   * @param int $timestamp
   *   The deployment creation timestamp.
   *
   * @return \Drupal\iots_deployment\IotsDeploymentInterface
   *   The called deployment entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the deployment status.
   *
   * @return bool
   *   TRUE if the deployment is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the deployment status.
   *
   * @param bool $status
   *   TRUE to enable this deployment, FALSE to disable.
   *
   * @return \Drupal\iots_deployment\IotsDeploymentInterface
   *   The called deployment entity.
   */
  public function setStatus($status);

}
