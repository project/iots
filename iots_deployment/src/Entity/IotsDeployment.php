<?php

namespace Drupal\iots_deployment\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\iots\Utility\FieldDefinition;
use Drupal\iots\Utility\ContentEntity;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the deployment entity class.
 *
 * @ContentEntityType(
 *   id = "iots_deployment",
 *   label = @Translation("Deployment"),
 *   label_collection = @Translation("Deployments"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\iots_deployment\IotsDeploymentListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\iots_deployment\Form\IotsDeploymentForm",
 *       "edit" = "Drupal\iots_deployment\Form\IotsDeploymentForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "iots_deployment",
 *   admin_permission = "administer deployment",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form"    = "/iots/deployment/add",
 *     "canonical"   = "/iots/deployment/{iots_device}",
 *     "edit-form"   = "/iots/deployment/{iots_device}/edit",
 *     "delete-form" = "/iots/deployment/{iots_device}/delete",
 *     "collection"  = "/iots/deployment"
 *   },
 *   field_ui_base_route = "entity.iots_deployment.settings"
 * )
 */
class IotsDeployment extends ContentEntity {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['title'] = FieldDefinition::string('Title');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom.
    $fields['state'] = FieldDefinition::list('Status', [
      'pending' => 'Pending',
      'active' => 'Active',
      'finish' => 'Finished',
      'delete' => 'Deleted',
    ]);
    $fields['state']->setDefaultValue('pending');
    $fields['ref_release'] = FieldDefinition::entity('Release', 'iots_release', TRUE);
    $fields['ref_release']->setRequired(TRUE);
    $fields['ref_device'] = FieldDefinition::entity('Device', 'iots_device');
    $fields['ref_device']->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $fields['ref_group'] = FieldDefinition::entity('Group', 'iots_group');
    $fields['ref_group']->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    $fields['comment'] = FieldDefinition::string('Comment');
    $fields['schedule'] = FieldDefinition::list('Schedule', [
      '_none' => 'Start immediately',
      'schedule' => 'Schedule a start date',
    ]);
    $fields['schedule_date'] = FieldDefinition::date('Schedule date');
    $fields['batch'] = FieldDefinition::list('Batch size', [
      '_none' => 'All',
      'percentage' => 'Percentage',
      'device' => 'Number of devices',
    ]);
    $fields['batch_value'] = FieldDefinition::int('Batch value');
    $fields['batch_delay'] = FieldDefinition::int('Batch delay minutes');
    // Info.
    $fields['start_date'] = FieldDefinition::date('Start date');
    $fields['finish_date'] = FieldDefinition::date('Finish date');
    $fields['yaml'] = FieldDefinition::long('Yaml');
    return $fields;
  }

}
