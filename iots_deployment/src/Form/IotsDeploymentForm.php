<?php

namespace Drupal\iots_deployment\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the deployment entity edit forms.
 */
class IotsDeploymentForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => \Drupal::service('renderer')->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New deployment %label has been created.', $message_arguments));
      $this->logger('iots_deployment')->notice('Created new deployment %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The deployment %label has been updated.', $message_arguments));
      $this->logger('iots_deployment')->notice('Updated new deployment %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.iots_deployment.canonical', ['iots_deployment' => $entity->id()]);
  }

}
