<?php

namespace Drupal\iots_layout\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the layout entity edit forms.
 */
class IotsLayoutForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => \Drupal::service('renderer')->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New layout %label has been created.', $message_arguments));
      $this->logger('iots_layout')->notice('Created new layout %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The layout %label has been updated.', $message_arguments));
      $this->logger('iots_layout')->notice('Updated new layout %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.iots_layout.canonical', ['iots_layout' => $entity->id()]);
  }

}
