<?php

namespace Drupal\iots_layout\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\iots\Utility\AjaxResult;

/**
 * Class EntityButton.
 */
class NewLayoutForm extends FormBase {

  /**
   * AJAX wrapper.
   *
   * @var string
   */
  private $wrapper = 'new-layout-form';

  /**
   * AJAX ajaxPrev.
   */
  public function ajaxSubmit(array &$form, $form_state) {
    $otvet = "ajaxSubmit:\n";
    $extra = $form_state->extra;
    if (is_numeric($extra['id'])) {
      $otvet .= "{$extra['id']}\n";
      $layout = [
        'product' => $extra['id'],
        'name' => trim($form_state->getValue('layout_name')),
        'description' => trim($form_state->getValue('layout_description')),
      ];
      \Drupal::service('iots_layout')->createNew($layout);
    }
    else {
      $otvet .= "ERR\n";
    }
    return AjaxResult::ajax($this->wrapper, $otvet);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL) {
    $wrap = $this->wrapper;
    $form_state->extra = $extra;
    $form_state->setCached(FALSE);
    $form['new-layout'] = [
      '#type' => 'details',
      '#title' => $this->t('Add New Layout'),
      '#open' => FALSE,
      "layout_iots_product" => [
        '#type' => 'hidden',
        '#value' => $extra['id'],
      ],
      "layout_name" => [
        '#title' => $this->t('Name'),
        '#type' => 'textfield',
        '#required' => FALSE,
      ],
      "layout_description" => [
        '#title' => $this->t('Description'),
        '#type' => 'textfield',
      ],
      "submit" => [
        '#type' => 'button',
        '#value' => $this->t('Submit'),
        '#attributes' => ['class' => ['inline']],
        '#ajax'   => [
          'callback' => '::ajaxSubmit',
          'effect'   => 'fade',
          'progress' => ['type' => 'throbber', 'message' => NULL],
        ],
        '#suffix' => "id:{$extra['id']}<div id='$wrap'></div>",
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'app-form';
  }

}
