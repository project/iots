<?php

namespace Drupal\iots_layout\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionLogEntityTrait;
use Drupal\iots\Utility\FieldDefinition;
use Drupal\iots\Utility\ContentEntity;
use Drupal\Core\Field\FieldStorageDefinitionInterface as FSDI;

/**
 * Defines the layout entity class.
 *
 * @ContentEntityType(
 *   id = "iots_layout",
 *   label = @Translation("Layout"),
 *   label_collection = @Translation("Layouts"),
 *   handlers = {
 *     "view_builder" = "Drupal\iots_layout\Entity\IotsLayoutViewBuilder",
 *     "list_builder" = "Drupal\iots_layout\Entity\IotsLayoutListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\iots_layout\Entity\IotsLayoutAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\iots_layout\Form\IotsLayoutForm",
 *       "edit" = "Drupal\iots_layout\Form\IotsLayoutForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "iots_layout",
 *   revision_table = "iots_layout_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer layout",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/iots/layout/add",
 *     "canonical" = "/iots/layout/{iots_layout}",
 *     "edit-form" = "/iots/layout/{iots_layout}/edit",
 *     "delete-form" = "/iots/layout/{iots_layout}/delete",
 *     "collection" = "/iots/layout"
 *   },
 *   field_ui_base_route = "entity.iots_layout.settings"
 * )
 */
class IotsLayout extends ContentEntity {

  use RevisionLogEntityTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields += static::revisionLogBaseFieldDefinitions($entity_type);
    $fields['uuid'] = FieldDefinition::unique();
    $fields['name'] = FieldDefinition::string('Name');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom.
    $fields['state'] = FieldDefinition::list('Mode', [
      'draft' => 'Draft',
      'production' => 'Production',
    ])
      ->setRequired(TRUE)
      ->setDefaultValue('draft');
    $fields['type'] = FieldDefinition::string('Type');
    $fields['version'] = FieldDefinition::string('Version');
    $fields['root_topic'] = FieldDefinition::string('Root Topic');
    $fields['description'] = FieldDefinition::string('Description');
    $fields['product'] = FieldDefinition::entity('Product', 'iots_product');
    $fields['widget'] = FieldDefinition::entity('Widgets', 'iots_widget')
      ->setCardinality(FSDI::CARDINALITY_UNLIMITED);
    // Info.
    $fields['yaml'] = FieldDefinition::long('Yaml');
    return $fields;
  }

}
