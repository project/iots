<?php

namespace Drupal\iots_layout\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Base class for entity view builders.
 *
 * @ingroup entity_api
 */
class IotsLayoutViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
    $build_list = $this->viewMultiple([$entity], $view_mode, $langcode);

    // The default ::buildMultiple() #pre_render callback won't run, because we
    // extract a child element of the default renderable array. Thus we must
    // assign an alternative #pre_render callback that applies the necessary
    // transformations and then still calls ::buildMultiple().
    $build = $build_list[0];
    $build['#pre_render'][] = [$this, 'build'];
    if ($view_mode == 'full' || $view_mode == 'default') {
      $uuid = $entity->uuid->value;
      $build['uuid'] = ['#markup' => "id:$uuid"];
      $data = \Drupal::service('iots_layout.data')->data($entity);
      $lib = 'react/react' . (\Drupal::currentUser()->id() == 1 ? '-dev' : NULL);
      $file = __CLASS__;
      $build['react'] = [
        '#markup' => "<div id='react-app'></div>",
        '#suffix' => "<pre>lib=$lib DISABLED: $file</pre>" ,
        '#attached' => [
          'drupalSettings' => [
            'iots' => [
              'layout' => [
                $uuid => $data,
              ],
            ],
          ],
          // 'library' => $lib,
        ],
      ];
    }
    return $build;
  }

}
