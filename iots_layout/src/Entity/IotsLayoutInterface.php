<?php

namespace Drupal\iots_layout\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a layout entity type.
 */
interface IotsLayoutInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the layout title.
   *
   * @return string
   *   Title of the layout.
   */
  public function getTitle();

  /**
   * Sets the layout title.
   *
   * @param string $title
   *   The layout title.
   *
   * @return \Drupal\iots_layout\IotsLayoutInterface
   *   The called layout entity.
   */
  public function setTitle($title);

  /**
   * Gets the layout creation timestamp.
   *
   * @return int
   *   Creation timestamp of the layout.
   */
  public function getCreatedTime();

  /**
   * Sets the layout creation timestamp.
   *
   * @param int $timestamp
   *   The layout creation timestamp.
   *
   * @return \Drupal\iots_layout\IotsLayoutInterface
   *   The called layout entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the layout status.
   *
   * @return bool
   *   TRUE if the layout is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the layout status.
   *
   * @param bool $status
   *   TRUE to enable this layout, FALSE to disable.
   *
   * @return \Drupal\iots_layout\IotsLayoutInterface
   *   The called layout entity.
   */
  public function setStatus($status);

}
