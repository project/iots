<?php

namespace Drupal\iots_layout\Entity;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the layout entity type.
 */
class IotsLayoutAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'view layout');

      case 'update':
        return AccessResult::allowedIfHasPermissions($account, ['edit layout', 'administer layout'], 'OR');

      case 'delete':
        return AccessResult::allowedIfHasPermissions($account, ['delete layout', 'administer layout'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create layout', 'administer layout'], 'OR');
  }

}
