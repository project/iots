<?php

namespace Drupal\iots_layout\Service;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\iots_layout\Entity\IotsLayout;

/**
 * IotsDevice service with settings.
 */
class LayoutData {
  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * The cml storage.
   *
   * @var \Drupal\iots_widget\Service\WidgetData
   */
  protected $widgetData;

  /**
   * Creates a new LayoutData Service.
   */
  public function __construct() {
    $this->widgetData = \Drupal::service('iots_widget.data');
  }

  /**
   * Creates a new Device Service.
   */
  public function data(IotsLayout $layout) : array {
    $widgets = [];
    foreach ($layout->widget as $field) {
      $uuid = $field->entity->uuid->value;
      $widgets[$uuid] = $this->widgetData->data($field->entity);
    }
    $result = [
      'uuid' => $layout->uuid->value,
      'name' => $layout->name->value,
      'widgets' => $widgets,
    ];
    return $result;
  }

}
