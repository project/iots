<?php

namespace Drupal\iots_layout\Service;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\iots_layout\Entity\IotsLayout;

/**
 * IotsLayout service with settings.
 */
class Layout {
  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Creates a new Layout Service.
   */
  public function __construct() {
    $entity_type = 'iots_layout';
    $this->storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    $this->entityType = \Drupal::entityTypeManager()->getDefinition($entity_type);
    $this->dateFormatter = \Drupal::service('date.formatter');
  }

  /**
   * Layout List.
   */
  public function layoutList(int $registry_id) : array {
    $title = t('Layouts');
    $rows = [];
    $no = 0;
    foreach ($this->query($registry_id) as $layout) {
      /** @var \Drupal\iots_layout\Entity\IotsLayout $layout */
      $rows[] = $this->buildRow($layout, ++$no);
    }
    $build['title'] = [
      '#markup' => "<h4>$title</h4>",
    ];
    $build['form'] = \Drupal::formBuilder()->getForm('Drupal\iots_layout\Form\NewLayoutForm', [
      'id' => $registry_id,
    ]);
    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->t('credentials'),
      '#rows' => $rows,
      '#empty' => $this->t('There are no @label yet.', [
        '@label' => $this->entityType->getPluralLabel(),
      ]),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];
    return $build;
  }

  /**
   * Device List.
   */
  public function createNew(array $user_input) {
    $name = substr($user_input['name'], 0, 60);
    if (!$name) {
      $name = \Drupal::service('iots.name')->random();
    }
    $description = substr($user_input['description'], 0, 255);

    // @todo Increment version.
    $version = '1.0';
    $layout = [
      'name' => $name,
      'description' => $description,
      'state' => 'draft',
      'version' => $version,
      'product' => $user_input['product'],
    ];
    $entity = $this->storage->create($layout);
    $entity->save();
    return $entity;
  }

  /**
   * Header.
   */
  private function buildHeader() {
    $header['no'] = $this->t('#');
    $header['name'] = $this->t('Name');
    $header['uuid'] = $this->t('ID');
    $header['version'] = $this->t('Version');
    $header['created'] = $this->t('Created');
    return $header;
  }

  /**
   * Row.
   */
  private function buildRow(IotsLayout $entity, $no = FALSE) {
    $row['no'] = $no;
    $row['name'] = $entity->toLink();
    $row['uuid'] = $entity->uuid->value;
    $status = '';
    if (!$entity->status->value) {
      $status = '<i class="far fa-eye-slash"></i>';
    }
    $state = $entity->state->value;
    $version = $entity->version->value;
    $row['version'] = [
      'data' => [
        '#markup' => "$status $state $version",
      ],
    ];
    $row['created'] = $this->dateFormatter->format($entity->getCreatedTime());
    return $row;
  }

  /**
   * Query.
   */
  private function query($product_id) {
    $entities = [];
    $query = $this->storage->getQuery()
      ->condition('status', 1)
      ->condition('product', $product_id)
      ->sort('created', 'ASC')
      ->range(0, 100);
    $ids = $query->execute();
    if (!empty($ids)) {
      foreach ($this->storage->loadMultiple($ids) as $id => $entity) {
        $entities[$id] = $entity;
      }
    }
    return $entities;
  }

}
