<?php

namespace Drupal\iots_product\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Base class for entity view builders.
 *
 * @ingroup entity_api
 */
class IotsProductViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
    $build_list = $this->viewMultiple([$entity], $view_mode, $langcode);

    // The default ::buildMultiple() #pre_render callback won't run, because we
    // extract a child element of the default renderable array. Thus we must
    // assign an alternative #pre_render callback that applies the necessary
    // transformations and then still calls ::buildMultiple().
    $build = $build_list[0];
    $build['#pre_render'][] = [$this, 'build'];
    $uuid = $entity->uuid->value;
    if ($view_mode == 'full') {
      // $build['uuid'] = ['#markup' => "<h4>ID: <small>$uuid</small></h4>"];
      if (\Drupal::hasService('iots_layout')) {
        $id = $entity->id();
        $build['layouts'] = \Drupal::service('iots_layout')->layoutList($id);
      }
    }
    return $build;
  }

}
