<?php

namespace Drupal\iots_product\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a product entity type.
 */
interface IotsProductInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the product title.
   *
   * @return string
   *   Title of the product.
   */
  public function getTitle();

  /**
   * Sets the product title.
   *
   * @param string $title
   *   The product title.
   *
   * @return \Drupal\iots_product\IotsProductInterface
   *   The called product entity.
   */
  public function setTitle($title);

  /**
   * Gets the product creation timestamp.
   *
   * @return int
   *   Creation timestamp of the product.
   */
  public function getCreatedTime();

  /**
   * Sets the product creation timestamp.
   *
   * @param int $timestamp
   *   The product creation timestamp.
   *
   * @return \Drupal\iots_product\IotsProductInterface
   *   The called product entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the product status.
   *
   * @return bool
   *   TRUE if the product is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the product status.
   *
   * @param bool $status
   *   TRUE to enable this product, FALSE to disable.
   *
   * @return \Drupal\iots_product\IotsProductInterface
   *   The called product entity.
   */
  public function setStatus($status);

}
