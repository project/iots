<?php

namespace Drupal\iots_product\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionLogEntityTrait;
use Drupal\iots\Utility\FieldDefinition;
use Drupal\iots\Utility\ContentEntity;
use Drupal\Core\Field\FieldStorageDefinitionInterface as FSDI;

/**
 * Defines the product entity class.
 *
 * @ContentEntityType(
 *   id = "iots_product",
 *   label = @Translation("Product"),
 *   label_collection = @Translation("Products"),
 *   handlers = {
 *     "view_builder" = "Drupal\iots_product\Entity\IotsProductViewBuilder",
 *     "list_builder" = "Drupal\iots_product\Entity\IotsProductListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\iots_product\Form\IotsProductForm",
 *       "edit" = "Drupal\iots_product\Form\IotsProductForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "iots_product",
 *   revision_table = "iots_product_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer product",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/iots/product/add",
 *     "canonical" = "/iots/product/{iots_product}",
 *     "edit-form" = "/iots/product/{iots_product}/edit",
 *     "delete-form" = "/iots/product/{iots_product}/delete",
 *     "collection" = "/iots/product"
 *   },
 *   field_ui_base_route = "entity.iots_product.settings"
 * )
 */
class IotsProduct extends ContentEntity {

  use RevisionLogEntityTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['uuid'] = FieldDefinition::unique();
    $fields['name'] = FieldDefinition::string('Name');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom Auth.
    $fields['credentials'] = FieldDefinition::entity('Credentials', 'iots_credentials')
      ->setCardinality(FSDI::CARDINALITY_UNLIMITED);
    // Info.
    $fields['yaml'] = FieldDefinition::long('Yaml');
    return $fields;
  }

}
