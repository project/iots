<?php

namespace Drupal\iots_credentials\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the device credentials entity edit forms.
 */
class IotsCredentialsForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => \Drupal::service('renderer')->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New device credentials %label has been created.', $message_arguments));
      $this->logger('iots_credentials')->notice('Created new device credentials %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The device credentials %label has been updated.', $message_arguments));
      $this->logger('iots_credentials')->notice('Updated new device credentials %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.iots_credentials.canonical', ['iots_credentials' => $entity->id()]);
  }

}
