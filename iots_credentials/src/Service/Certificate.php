<?php

namespace Drupal\iots_credentials\Service;

use Symfony\Component\Process\Process;

/**
 * Iots Certificate service.
 */
class Certificate {

  /**
   * Creates a new Certificate service.
   */
  public function __construct() {

  }

  /**
   * Exec.
   */
  public function exec($cmd) {
    $process = new Process($cmd);
    $process->setTimeout(2);
    $process->start();
    $process->wait();
    return $process->getOutput();
  }

  /**
   * GET!
   */
  public function genSsh($email) {
    $dir = $this->createTempDirecory();
    $key = "$dir/key";
    $this->exec("ssh-keygen -t rsa -C $email -f $key.pem");
    $this->exec("puttygen $key.pem -o $key.ppk");
    $this->ppk = $this->exec("cat $key.ppk");
    $this->pem = $this->exec("cat $key.pem");
    $this->pub = $this->exec("cat $key.pem.pub");
    $this->exec("rm $dir/*");
    $this->exec("rm $dir -r");
    return $this;
  }

  /**
   * OpenSSL0 2048/4096.
   */
  public function genOpenSsl($name, $x = 4096) {
    $dir = $this->createTempDirecory();
    $key = "$dir/key";
    $days = "-days 7300";
    $keys = "-keyout $key.pem -out $dir/cert.pem";
    $this->exec("openssl req -x509 -nodes -newkey rsa:$x $days $keys -subj '/CN=$name'");
    $this->exec("ssh-keygen -f $key.pem -y > $key.pub");
    $this->pem = $this->exec("cat $key.pem");
    $this->pub = $this->exec("cat $key.pub");
    $this->cert = $this->exec("cat $dir/cert.pem");
    $this->exec("rm $dir/*");
    $this->exec("rm $dir -r");
    return $this;
  }

  /**
   * Create Temp Directory.
   */
  private function createTempDirecory() {
    $rand = mt_rand();
    $dir = "~/.cert-keys/key-$rand";
    $this->exec("mkdir -p $dir");
    return $dir;
  }

}
