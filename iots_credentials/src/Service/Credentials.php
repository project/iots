<?php

namespace Drupal\iots_credentials\Service;

use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\iots_credentials\Entity\IotsCredentials;

/**
 * Iots Certificate service.
 */
class Credentials {
  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Creates a new Credentials service.
   */
  public function __construct() {
    $entity_type = 'iots_credentials';
    $this->storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    $this->entityType = \Drupal::entityTypeManager()->getDefinition($entity_type);
    $this->dateFormatter = \Drupal::service('date.formatter');
  }

  /**
   * Render.
   */
  public function render(array $ids) : array {
    $title = t('Credentials');
    $build['title'] = [
      '#markup' => "<h4>$title</h4>",
    ];
    $rows = [];
    foreach ($this->storage->loadMultiple($ids) as $credential) {
      /** @var \Drupal\iots_credentials\Entity\IotsCredentials $credential */
      $rows[] = $this->buildRow($credential);
    }
    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->t('Credentials'),
      '#rows' => $rows,
      '#empty' => $this->t('There are no @label yet.', [
        '@label' => $this->entityType->getPluralLabel(),
      ]),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];
    $build['#attached'] = [
      'library' => ['iots/copy_text'],
    ];
    return $build;
  }

  /**
   * Header.
   */
  private function buildHeader() {
    $header['uuid'] = $this->t('ID');
    $header['type'] = $this->t('Type');
    $header['credential'] = $this->t('Credential');
    $header['uid'] = $this->t('Author');
    $header['created'] = $this->t('Created');
    $header['view'] = $this->t('View');
    return $header;
  }

  /**
   * Row.
   */
  private function buildRow(IotsCredentials $entity) {
    $row['uuid'] = $entity->uuid->value;
    $row['type'] = $entity->type->value;
    if ($entity->type->value == 'rsa') {
      $cert = $entity->rsa_cert->value;
    }
    else {
      $cert = $entity->password->value;
    }
    $copy = '<i class="far fa-copy"></i>';
    $row['credential'] = [
      'data' => [
        '#markup' => "<span class='cert__text'>$cert</span><span class='copy__btn'>$copy</span>",
      ],
      'class' => ['cert__table-cell'],
    ];
    $row['uid']['data'] = [
      '#theme' => 'username',
      '#account' => $entity->getOwner(),
    ];
    $row['created'] = $this->dateFormatter->format($entity->getCreatedTime());
    $url = $entity->toLink('canonical')->getUrl();
    $row['view'] = Link::fromTextAndUrl($this->t('View'), $url);
    return $row;
  }

}
