<?php

namespace Drupal\iots_credentials\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\iots\Utility\FieldDefinition;
use Drupal\iots\Utility\ContentEntity;
use Drupal\Component\Utility\Crypt;

/**
 * Defines the device credentials entity class.
 *
 * @ContentEntityType(
 *   id = "iots_credentials",
 *   label = @Translation("IOTs Credentials"),
 *   label_collection = @Translation("IOTs Credentialss"),
 *   handlers = {
 *     "view_builder" = "Drupal\iots_credentials\Entity\IotsCredentialsViewBuilder",
 *     "list_builder" = "Drupal\iots_credentials\Entity\IotsCredentialsListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\iots_credentials\Form\IotsCredentialsForm",
 *       "edit" = "Drupal\iots_credentials\Form\IotsCredentialsForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "iots_credentials",
 *   admin_permission = "administer device credentials",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form"    = "/iots/credentials/add",
 *     "canonical"   = "/iots/credentials/{iots_credentials}",
 *     "edit-form"   = "/iots/credentials/{iots_credentials}/edit",
 *     "delete-form" = "/iots/credentials/{iots_credentials}/delete",
 *     "collection"  = "/iots/credentials"
 *   },
 *   field_ui_base_route = "entity.iots_credentials.settings"
 * )
 */
class IotsCredentials extends ContentEntity {

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    $type = $this->type->value;
    $human_type = ucfirst($type);
    $timestamp = $this->created->value;
    $time = \Drupal::service('date.formatter')->format($timestamp, 'custom', 'd.m.Y H:i');
    $this->name->setValue("$human_type - $time");
    if ($type == 'rsa' && !$this->rsa_pem->value) {
      $uid = \Drupal::currentUser()->id();
      $cert = \Drupal::service('iots_credentials.certificate')->genOpenSsl("$uid@iots");
      $this->rsa_pem->setValue($cert->pem);
      $this->rsa_pub->setValue($cert->pub);
      $this->rsa_cert->setValue($cert->cert);
    }
    if ($type == 'password' && !$this->password->value) {
      $pass = Crypt::randomBytesBase64(16);
      $this->password->setValue($pass);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $self = "Drupal\iots_credentials\Entity\IotsCredentials";
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['uuid'] = FieldDefinition::unique();
    $fields['name'] = FieldDefinition::string('Name');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom.
    $fields['type'] = FieldDefinition::list('Type', [
      'rsa' => 'RSA Certificate',
      'password' => 'Password',
    ])
      ->setRequired(TRUE)
      ->setDefaultValue('certificate');
    $fields['password'] = FieldDefinition::string('Password');
    $fields['rsa_pem'] = FieldDefinition::long('RSA');
    $fields['rsa_pub'] = FieldDefinition::long('PRS.pub');
    $fields['rsa_cert'] = FieldDefinition::long('RSA.Cert');
    return $fields;
  }

}
