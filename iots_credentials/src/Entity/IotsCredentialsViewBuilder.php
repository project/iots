<?php

namespace Drupal\iots_credentials\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Base class for entity view builders.
 *
 * @ingroup entity_api
 */
class IotsCredentialsViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
    $build_list = $this->viewMultiple([$entity], $view_mode, $langcode);
    $build = $build_list[0];
    $build['#pre_render'][] = [$this, 'build'];
    $uuid = $entity->uuid->value;
    $build['uuid'] = ['#markup' => "<h4>ID: <small>$uuid</small></h4>"];

    return $build;
  }

}
