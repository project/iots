<?php

namespace Drupal\iots_credentials\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a device credentials entity type.
 */
interface IotsCredentialsInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the device credentials title.
   *
   * @return string
   *   Title of the device credentials.
   */
  public function getTitle();

  /**
   * Sets the device credentials title.
   *
   * @param string $title
   *   The device credentials title.
   *
   * @return \Drupal\iots_credentials\Entity\IotsCredentialsInterface
   *   The called device credentials entity.
   */
  public function setTitle($title);

  /**
   * Gets the device credentials creation timestamp.
   *
   * @return int
   *   Creation timestamp of the device credentials.
   */
  public function getCreatedTime();

  /**
   * Sets the device credentials creation timestamp.
   *
   * @param int $timestamp
   *   The device credentials creation timestamp.
   *
   * @return \Drupal\iots_credentials\Entity\IotsCredentialsInterface
   *   The called device credentials entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the device credentials status.
   *
   * @return bool
   *   TRUE if the device credentials is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the device credentials status.
   *
   * @param bool $status
   *   TRUE to enable this device credentials, FALSE to disable.
   *
   * @return \Drupal\iots_credentials\Entity\IotsCredentialsInterface
   *   The called device credentials entity.
   */
  public function setStatus($status);

}
