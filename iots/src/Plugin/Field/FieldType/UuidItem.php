<?php

namespace Drupal\iots\Plugin\Field\FieldType;

use RandomLib\Factory;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;

/**
 * Defines the 'unique' entity field type.
 *
 * The field uses a newly generated UUID as default value.
 *
 * @FieldType(
 *   id = "unique",
 *   label = @Translation("UUID"),
 *   description = @Translation("An entity field containing a UUID."),
 *   no_ui = TRUE,
 *   default_formatter = "string"
 * )
 */
class UuidItem extends StringItem {

  /**
   * String Generator.
   */
  public static function gen($length = 20) {
    $factory = new Factory();
    $chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    $generator = $factory->getMediumStrengthGenerator();
    $unique = $generator->generateString($length, $chars);
    return $unique;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'max_length' => 128,
      'is_ascii' => TRUE,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function applyDefaultValue($notify = TRUE) {
    $unique = self::gen();
    $this->setValue(['value' => $unique], $notify);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);
    $schema['unique keys']['value'] = ['value'];
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['value'] = self::gen();
    return $values;
  }

}
