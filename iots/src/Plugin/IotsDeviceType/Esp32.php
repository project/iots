<?php

namespace Drupal\iots\Plugin\IotsDeviceType;

use Drupal\iots\PluginManager\DeviceTypePluginBase;
use Drupal\iots\PluginManager\DeviceTypePluginInterface;

/**
 * Provides a 'Esp32' Template.
 *
 * @DeviceTypeAnnotation(
 *   id = "esp32",
 *   title = @Translation("Esp32"),
 * )
 */
class Esp32 extends DeviceTypePluginBase implements DeviceTypePluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->board = "esp32";
  }

}
