<?php

namespace Drupal\iots\Utility;

/**
 * @file
 * Contains \Drupal\iots\Utility\DrushExec.
 */

/**
 * Utility for drush.
 *
 * Method: exec() - exec with drush.
 */
class DrushExec {

  /**
   * Exec.
   */
  public static function exec($cmd, $mode = 'nohup', $update = FALSE) {
    $result = FALSE;
    $log = "~/iots/nohup-iots.out 2> ~/iots/nohup-iots.err < /dev/null &";
    if (TRUE) {
      $drush = self::getDrush();
      $root = DRUPAL_ROOT;
      if ($update) {
        $cmd = "$cmd  --update";
      }
      $result .= "CMD: $cmd\n";
      if ($mode == 'nohup') {
        $cmd = "nohup nice -n -5 {$drush} {$cmd} --root=$root > $log";
        $result .= "\nEXEC: $cmd\n";
        \Drupal::logger(__CLASS__)->notice("exec: $cmd");
        exec($cmd);
      }
      elseif ($mode == 'exec') {
        $result .= "$cmd\n";
        $result .= shell_exec($cmd);
      }
      elseif ($mode == 'drush') {
        $cmd = "drush --version";
        $result .= "\nEXEC: $cmd\n";
        $result .= shell_exec($cmd);
      }
      elseif ($mode == 'debug') {
        $cmd = "ls";
        $result .= "\nEXEC: $cmd\n";
        $result .= shell_exec($cmd);
      }
    }
    return $result;
  }

  /**
   * Drush.
   */
  public static function getDrush($config = FALSE) {
    $drush = "/usr/local/bin/drush";
    if (is_object($config) && $config->get('drush')) {
      $drush = $config->get('drush');
    }
    return $drush;
  }

}
