<?php

namespace Drupal\iots\Utility;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * @file
 * Contains \Drupal\iots\Utility\FieldDefinition.
 */

/**
 * Field Definition.
 */
class FieldDefinition {

  /**
   * String.
   */
  public static function string($lable, $required = FALSE) : BaseFieldDefinition {
    return BaseFieldDefinition::create('string')
      ->setLabel($lable)
      ->setRequired($required)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * Long String.
   */
  public static function long($lable) : BaseFieldDefinition {
    return BaseFieldDefinition::create('string_long')
      ->setLabel($lable)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 60,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 60,
      ])
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * Status.
   */
  public static function status() : BaseFieldDefinition {
    return BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 99,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 99,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * Uid.
   */
  public static function uid() : BaseFieldDefinition {
    return BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the nashdom author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 80,
      ])
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * Created.
   */
  public static function created() : BaseFieldDefinition {
    return BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the nashdom was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 70,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * Changed.
   */
  public static function changed() : BaseFieldDefinition {
    return BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the nashdom was last edited.'));
  }

  /**
   * List String.
   */
  public static function list($lable, $options) : BaseFieldDefinition {
    return BaseFieldDefinition::create('list_string')
      ->setLabel($lable)
      ->setSettings([
        'allowed_values' => $options,
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 10,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * List String.
   */
  public static function int($lable) : BaseFieldDefinition {
    return BaseFieldDefinition::create('integer')
      ->setLabel($lable)
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'number',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * Boolean.
   */
  public static function bool($lable) : BaseFieldDefinition {
    return BaseFieldDefinition::create('boolean')
      ->setLabel($lable)
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
      ]);
  }

  /**
   * List String.
   */
  public static function date($lable) : BaseFieldDefinition {
    return BaseFieldDefinition::create('datetime')
      ->setLabel($lable)
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'datetime_default',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * Entity Ref.
   */
  public static function entity($label, $type, $required = FALSE) : BaseFieldDefinition {
    return BaseFieldDefinition::create('entity_reference')
      ->setLabel($label)
      ->setSetting('target_type', $type)
      ->setSetting('handler', 'default')
      ->setRequired($required)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => 40,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 40,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  }

  /**
   * Map.
   */
  public static function map($lable) : BaseFieldDefinition {
    return BaseFieldDefinition::create('map')
      ->setLabel($lable)
      ->setDescription(t('A serialized array.'));
  }

  /**
   * Decimal.
   */
  public static function decimal($label) : BaseFieldDefinition {
    return BaseFieldDefinition::create('decimal')
      ->setLabel($label)
      ->setSettings([
        'precision' => 19,
        'scale' => 6,
      ]);
  }

  /**
   * UUID > unique.
   */
  public static function unique() : BaseFieldDefinition {
    return BaseFieldDefinition::create('unique')
      ->setLabel(new TranslatableMarkup('UUID'))
      ->setReadOnly(TRUE);
  }

}
