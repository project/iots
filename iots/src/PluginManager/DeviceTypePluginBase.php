<?php

namespace Drupal\iots\PluginManager;

use Drupal\Component\Plugin\PluginBase;

/**
 * Provides an DeviceType plugin manager.
 *
 * @see Drupal\iots\PluginManager\DeviceTypeAnnotation
 * @see Drupal\iots\PluginManager\DeviceTypePluginInterface
 * @see plugin_api
 */
class DeviceTypePluginBase extends PluginBase implements DeviceTypePluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->board = FALSE;
  }

}
