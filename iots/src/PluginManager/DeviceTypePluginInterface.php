<?php

namespace Drupal\iots\PluginManager;

/**
 * Defines the common interface for all DeviceType classes.
 *
 * @see \Drupal\iots\PluginManager\DeviceTypeManager
 * @see \Drupal\iots\PluginManager\DeviceTypeAnnotation
 * @see plugin_api
 */
interface DeviceTypePluginInterface {

}
