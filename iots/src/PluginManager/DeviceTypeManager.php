<?php

namespace Drupal\iots\PluginManager;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an Archiver plugin manager.
 *
 * @see \Drupal\iots\PluginManager\DeviceTypePluginInterface
 * @see \Drupal\iots\PluginManager\DeviceTypeManager
 * @see plugin_api
 */
class DeviceTypeManager extends DefaultPluginManager {

  /**
   * Constructs a ArchiverManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/IotsDeviceType',
      $namespaces,
      $module_handler,
      'Drupal\iots\PluginManager\DeviceTypePluginInterface',
      'Drupal\iots\PluginManager\DeviceTypeAnnotation'
    );
    $this->alterInfo('iots_info');
    $this->setCacheBackend($cache_backend, 'iots_device_type_plugin');
    $this->factory = new DefaultFactory($this->getDiscovery());
  }

  /**
   * DeviceType - Plugable Options.
   */
  public function optionsList() {
    $options = [];
    foreach ($this->getDefinitions() as $key => $plugin) {
      $options[$key] = $plugin['title']->__toString();
    }
    return $options;
  }

}
