<?php

namespace Drupal\iots\PluginManager;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an iots template object.
 *
 * Plugin Namespace: Plugin\DeviceType.
 *
 * For a working example, see \Drupal\system\Plugin\DeviceType\MySql
 *
 * @see \Drupal\iots\PluginManager\DeviceTypeManager
 * @see \Drupal\iots\PluginManager\DeviceTypePluginInterface
 * @see plugin_api
 * @see hook_archiver_info_alter()
 *
 * @Annotation
 */
class DeviceTypeAnnotation extends Plugin {

  /**
   * The archiver plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the archiver plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

}
