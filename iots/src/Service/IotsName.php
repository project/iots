<?php

namespace Drupal\iots\Service;

/**
 * @file
 * Contains \Drupal\iots\Service\IotsName.
 */

/**
 * Controller routines for page example routes.
 */
class IotsName {

  /**
   * Gen!
   */
  public function random($lang = 'ru') {
    $type   = $this->getType($lang);
    $animal = $this->getAnimal($lang);
    if (strpos($animal, '|ж')) {
      $type = str_replace(['ой', 'ий', 'ый'], 'ая', $type);
      $animal = str_replace('|ж', '', $animal);
    }

    $name = "$type $animal";
    $name = mb_convert_case($name, MB_CASE_TITLE, "UTF-8");
    return $name;
  }

  /**
   * Type!
   */
  private function getType($lang = 'ru') {
    $string = $this->dataType();
    $arr = explode("\n", $string);
    $rand_key = array_rand($arr);
    $rand_val = $arr[$rand_key];
    if ($lang == 'en') {
      $val = strstr($rand_val, ' — ', TRUE);
    }
    else {
      $val = strstr($rand_val, ' — ');
      $val = str_replace(' — ', '', $val);
    }
    return $val;
  }

  /**
   * Animal!
   */
  private function getAnimal($lang = 'ru') {
    $string = $this->dataAnimal();

    $arr = explode("\n", $string);
    $rand_key = array_rand($arr);
    $rand_val = $arr[$rand_key];

    if ($lang == 'en') {
      $val = strstr($rand_val, ' - ', TRUE);
    }
    else {
      $val = strstr($rand_val, ' - ');
      $val = str_replace(' - ', '', $val);
    }
    return $val;
  }

  /**
   * Type data.
   */
  private function dataType() {
    return 'aromatic — ароматный
blunt — тупой
calm — тихий
cold — холодный
cool — прохладный
deaf — глухой
deep — глубокий
dry — сухой
far — далекий
plane — плоский
fragrant — душистый
fresh — свежий
hard — твердый
hot — горячий
huge — огромный
big — большой
small — маленький
long — длинный
loud — громкий
low — низкий
noisy — шумный
pleasant — приятный
quiet — тихий
rough — грубый
shallow — мелкий
sharp — резкий
short — короткий
smooth — гладкий
soft — мягкий
strong — крепкий
sweet — сладкий
tall — высокий
thick — толстый
thin — тонкий
warm — теплый
wet — мокрый
wide — широкий
angry — сердитый
angst — испуганный
awful — ужасный
bad — плохой
lovely — красивый
bored — скучающий
cheerful — веселый
cheery — радостный
dangerous — опасный
dreamy — мечтательный
excellent — отличный
excited — возбужденный
glad — довольный
good — хороший
haggard — изможденный
happy — счастливый
hysterical — истеричный
interesting — интересный
joyful — радостный
lucky — везучий
nice — милый
normal — обычный
offended — оскорбленный
optimistic — оптимистический
perfect — совершенный
pretty — привлекательный
sad — печальный
shocked — шокированный
strange — странный
superior — превосходный
surprised — удивленный
ugly — безобразный
upset — расстроенный
wonderful — чудесный
woozy — одурманенный
worried — обеспокоенный
baked — печеный
busy — занятый
clean — чистый
comfortable — удобный
complete — завершенный
damaged — поврежденный
dirty — грязный
engaged — занятый
fat — жирный
healthy — здоровый
hungry — голодный
married — женатый
rich — богатый
sick — больной
single — холостой
slim — стройный
strong — сильный
tired — усталый
vacant — свободный
weak — слабый
young — молодой
brave — храбрый
calm — спокойный
cold — холодный
creative — творческий
curious — любопытный
emotional — эмоциональный
friendly — дружелюбный
greedy — жадный
guilty — виновный
honest — честный
lonely — одинокий
modest — скромный
nervous — нервный
optimistic — оптимистичный
practical — практичный
selfish — эгоистичный
serious — серьезный
shy — застенчивый
average — средний
correct — правильный
empty — пустой
equal — равный
illegal — незаконный
wrong — неправильный
cheap — дешевый
expensive — дорогой
invalid — недействительный
priceless — бесценный
special — специальный
great — гениальный
lazy — ленивый
mad — сумасшедший
little — маленький
rare — редкий
slow — медленный
sudden — внезапный
first — первый
last — последний
young — молодой';
  }

  /**
   * Animal.
   */
  private function dataAnimal() {
    return 'stork - аист
shark - акула|ж
antelope - антилопа|ж
butterfly - бабочка|ж
ram - баран
badger - барсук
squirrel - белка|ж
beaver - бобр
bull - бык
wolf - волк
budgie - попугайчик
sparrow - воробей
raven - ворон
crow - ворона|ж
viper - гадюка|ж
pigeon,  - голубь
gorilla - горилла|ж
goose - гусь
boar - кабан
blackbird - дрозд
woodpecker - дятел
hedgehog - ёж
lark - жаворонок
giraffe - жираф
beetle - жук
crane - журавль
hare - заяц
zebra - зебра|ж
snake - змея|ж
turkey - индюк
canary - канарейка|ж
kangaroo - кенгуру|ж
goat - козел
gnat - комар
cow - корова|ж
cat - кот
kitten - котенок
crocodile - крокодил
rabbit - кролик
rat - крыса|ж
cuckoo - кукушка|ж
аpartridge - куропатка|ж
swallow - ласточка|ж
swan - лебедь
lion - лев
fox - лисица|ж
elk - лось
horse - лошадь|ж
frog - лягушка|ж
bear - медведь
moth - моль|ж
ant - муравей
fly - муха|ж
mouse - мышь|ж
rhino - носорог
monkey - обезьяна|ж
sheep - овца|ж
deer - олень
donkey - осел
peacock - павлин
spider - паук
cock - петух
penguin - пингвин
leech - пиявка|ж
pony - пони
parrot - попугай
pig - поросенок
bee - пчела|ж
lynx - рысь|ж
titmouse - синица|ж
scorpion - скорпион
elephant - слон
dog - собака|ж
owl - сова|ж
falcon - сокол
nightingale - соловей
magpie - сорока|ж
ostrich - страус
calf - теленок
tiger - тигр
seal - тюлень
snail - улитка|ж
duckling - утенок
duck - утка|ж
pheasant - фазан
flamingo - фламинго
ferret - хорёк
heron - цапля|ж
chic - цыпленок
seagull - чайка|ж
worm - червь
tortoise - черепаха|ж
puppy - щенок
lamb - ягненок
hawk - ястреб
lizard - ящерица|ж';
  }

}
