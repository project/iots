<?php

namespace Drupal\iots\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Debug.
 */
class IotsDebug extends ControllerBase {

  /**
   * Debug page.
   */
  public function page($test = 'hello') {
    $df = \Drupal::service('date.formatter');
    $msg = [
      'status' => 'ok',
      'time' => \Drupal::time()->getRequestTime(),
      'htime' => $df->format(time(), 'custom', 'dM H:i:s'),
    ];
    $json = json_encode($msg, JSON_UNESCAPED_UNICODE);
    $response = new Response($json);
    $response->headers->set('Content-Type', 'application/json');
    return $response;
  }

}
