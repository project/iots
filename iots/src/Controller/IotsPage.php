<?php

namespace Drupal\iots\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class Dashboard.
 */
class IotsPage extends ControllerBase {

  /**
   * Dashboard Page.
   */
  public function page() {
    return [
      '#markup' => 'IOTs TODO',
    ];
  }

}
