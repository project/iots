/**
 * @file
 * Iots certs.
 */

(function ($) {
  $(document).ready(function () {
    ("use strict");
    const elements = document.querySelectorAll(".copy__btn");
    const each = Array.prototype.forEach;
    each.call(elements, (el, i) => {
      el.addEventListener("click", function (event) {
        event.preventDefault();
        const text = event.target.parentElement.parentElement.textContent;
        navigator.clipboard.writeText(text);
      });
    });
  });
})(this.jQuery);
