CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The IOTs module provides extra settings for emails.

 * For a full description of the module visit:
   https://www.drupal.org/project/iots

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/iots


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the IOTs module as you would normally install a contributed
   Drupal module. Visit https://www.drupal.org/node/1897420 for further
   information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > System > IOTs.
    4. Configure & Save.


MAINTAINERS
-----------

 * Anatoly Politsin (APolitsin) - https://www.drupal.org/u/apolitsin

Supporting organization:

 * Synapse-studio - https://www.drupal.org/synapse-studio
