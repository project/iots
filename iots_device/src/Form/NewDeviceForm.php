<?php

namespace Drupal\iots_device\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\iots\Utility\AjaxResult;

/**
 * Class EntityButton.
 */
class NewDeviceForm extends FormBase {
  private $wrapper = 'new-device-form';

  /**
   * AJAX ajaxPrev.
   */
  public function ajaxSubmit(array &$form, $form_state) {
    $otvet = "ajaxSubmit:\n";
    $extra = $form_state->extra;
    if (is_numeric($extra['id'])) {
      $otvet .= "{$extra['id']}\n";
      $device = [];
      foreach ($form_state->getValues() as $key => $value) {
        if (substr($key, 0, 7) == 'device_') {
          $k = substr($key, 7);
          $device[$k] = $value;
        }
      }
      \Drupal::service('iots_device')->createNew($device);
    }
    else {
      $otvet .= "ERR\n";
    }
    return AjaxResult::ajax($this->wrapper, $otvet);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL) {
    $wrap = $this->wrapper;
    $form_state->extra = $extra;
    $form_state->setCached(FALSE);
    $form['new-device'] = [
      '#type' => 'details',
      '#title' => $this->t('Add New Device'),
      '#open' => FALSE,
      "device_registry" => [
        '#type' => 'hidden',
        '#value' => $extra['id'],
      ],
      "device_name" => [
        '#title' => $this->t('Name'),
        '#type' => 'textfield',
        '#required' => TRUE,
      ],
      "device_description" => [
        '#title' => $this->t('Description'),
        '#type' => 'textfield',
      ],
      "device_type" => [
        '#title' => $this->t('Type'),
        '#type' => 'select',
        '#options' => \Drupal::service('plugin.manager.iots_type_device')->optionsList(),
      ],
      "device_credentials" => [
        '#title' => $this->t('Auth'),
        '#type' => 'checkboxes',
        '#options' => [
          'rsa' => 'RSA Certificate',
          'password' => 'Password',
        ],
      ],
      "submit" => [
        '#type' => 'button',
        '#value' => $this->t('Submit'),
        '#attributes' => ['class' => ['inline']],
        '#ajax'   => [
          'callback' => '::ajaxSubmit',
          'effect'   => 'fade',
          'progress' => ['type' => 'throbber', 'message' => NULL],
        ],
        '#suffix' => "id:{$extra['id']}<div id='$wrap'></div>",
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'app-form';
  }

}
