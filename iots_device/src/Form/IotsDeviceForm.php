<?php

namespace Drupal\iots_device\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the device entity edit forms.
 */
class IotsDeviceForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => \Drupal::service('renderer')->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New device %label has been created.', $message_arguments));
      $this->logger('iots_device')->notice('Created new device %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The device %label has been updated.', $message_arguments));
      $this->logger('iots_device')->notice('Updated new device %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.iots_device.canonical', ['iots_device' => $entity->id()]);
  }

}
