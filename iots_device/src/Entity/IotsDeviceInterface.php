<?php

namespace Drupal\iots_device\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a device entity type.
 */
interface IotsDeviceInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the device name.
   *
   * @return string
   *   Title of the device.
   */
  public function getTitle();

  /**
   * Sets the device name.
   *
   * @param string $title
   *   The device name.
   *
   * @return \Drupal\iots_device\Entity\IotsDeviceInterface
   *   The called device entity.
   */
  public function setTitle($title);

  /**
   * Gets the device creation timestamp.
   *
   * @return int
   *   Creation timestamp of the device.
   */
  public function getCreatedTime();

  /**
   * Sets the device creation timestamp.
   *
   * @param int $timestamp
   *   The device creation timestamp.
   *
   * @return \Drupal\iots_device\Entity\IotsDeviceInterface
   *   The called device entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the device status.
   *
   * @return bool
   *   TRUE if the device is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the device status.
   *
   * @param bool $status
   *   TRUE to enable this device, FALSE to disable.
   *
   * @return \Drupal\iots_device\Entity\IotsDeviceInterface
   *   The called device entity.
   */
  public function setStatus($status);

}
