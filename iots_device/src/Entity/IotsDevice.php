<?php

namespace Drupal\iots_device\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\iots\Utility\FieldDefinition;
use Drupal\iots\Utility\ContentEntity;
use Drupal\Core\Field\FieldStorageDefinitionInterface as FSDI;

/**
 * Defines the device entity class.
 *
 * @ContentEntityType(
 *   id = "iots_device",
 *   label = @Translation("Device"),
 *   label_collection = @Translation("Devices"),
 *   handlers = {
 *     "view_builder" = "Drupal\iots_device\Entity\IotsDeviceViewBuilder",
 *     "list_builder" = "Drupal\iots_device\Entity\IotsDeviceListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\iots_device\Form\IotsDeviceForm",
 *       "edit" = "Drupal\iots_device\Form\IotsDeviceForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "iots_device",
 *   revision_table = "iots_device_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer device",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form"    = "/iots/device/add",
 *     "canonical"   = "/iots/device/{iots_device}",
 *     "edit-form"   = "/iots/device/{iots_device}/edit",
 *     "delete-form" = "/iots/device/{iots_device}/delete",
 *     "collection"  = "/iots/device"
 *   },
 *   field_ui_base_route = "entity.iots_device.settings"
 * )
 */
class IotsDevice extends ContentEntity {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['uuid'] = FieldDefinition::unique();
    $fields['name'] = FieldDefinition::string('Name');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom.
    $fields['state'] = FieldDefinition::list('Mode', [
      'pending' => 'Pending',
      'accepted' => 'Accepted',
      'rejected' => 'Rejected',
      'dismissed' => 'Dismissed',
      'deleted' => 'Deleted',
    ])
      ->setRequired(TRUE)
      ->setDefaultValue('pending');
    $types = \Drupal::service('plugin.manager.iots_type_device')->optionsList();
    $fields['type'] = FieldDefinition::list('Type', $types);
    $fields['key'] = FieldDefinition::string('Key');
    $fields['mac'] = FieldDefinition::string('MAC');
    $fields['version'] = FieldDefinition::string('Current Version');
    $fields['registry'] = FieldDefinition::entity('Registry', 'iots_registry');
    $fields['description'] = FieldDefinition::string('Description');
    // SSH.
    $fields['credentials'] = FieldDefinition::entity('Credentials', 'iots_credentials')
      ->setCardinality(FSDI::CARDINALITY_UNLIMITED);
    // Info.
    $fields['first_date'] = FieldDefinition::date('First request');
    $fields['last_date'] = FieldDefinition::date('Last check-in');
    $fields['yaml'] = FieldDefinition::long('Yaml');
    return $fields;
  }

}
