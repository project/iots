<?php

namespace Drupal\iots_device\Service;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\iots_device\Entity\IotsDevice;

/**
 * IotsDevice service with settings.
 */
class Device {
  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Creates a new Device Service.
   */
  public function __construct() {
    $entity_type = 'iots_device';
    $this->storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    $this->entityType = \Drupal::entityTypeManager()->getDefinition($entity_type);
    $this->dateFormatter = \Drupal::service('date.formatter');
  }

  /**
   * Device List.
   */
  public function deviceList(int $registry_id) : array {
    $title = t('Devices');
    $rows = [];
    $no = 0;
    foreach ($this->query($registry_id) as $device) {
      /** @var \Drupal\iots_device\Entity\IotsDevice $device */
      $rows[] = $this->buildRow($device, ++$no);
    }
    $build['title'] = [
      '#markup' => "<h4>$title</h4>",
    ];
    $build['form'] = \Drupal::formBuilder()->getForm('Drupal\iots_device\Form\NewDeviceForm', [
      'id' => $registry_id,
    ]);
    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->t('credentials'),
      '#rows' => $rows,
      '#empty' => $this->t('There are no @label yet.', [
        '@label' => $this->entityType->getPluralLabel(),
      ]),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];
    return $build;
  }

  /**
   * Device List.
   */
  public function createNew($fromValues) {
    $storageCert = \Drupal::entityTypeManager()->getStorage('iots_credentials');
    if (!empty($fromValues['credentials'])) {
      foreach ($fromValues['credentials'] as $key => $value) {
        if ($value) {
          $cert = $storageCert->create([
            'type' => $key,
          ]);
          $cert->save();
          $fromValues['credentials'][] = $cert->id();
        }
        unset($fromValues['credentials'][$key]);
      }
    }
    $entity = $this->storage->create($fromValues);
    $entity->save();
    return $entity;
  }

  /**
   * Header.
   */
  private function buildHeader() {
    $header['no'] = $this->t('#');
    $header['name'] = $this->t('Name');
    $header['uuid'] = $this->t('ID');
    $header['type'] = $this->t('Type');
    $header['state'] = $this->t('State');
    $header['uid'] = $this->t('Author');
    $header['created'] = $this->t('Created');
    return $header;
  }

  /**
   * Row.
   */
  private function buildRow(IotsDevice $entity, $no = FALSE) {
    $row['no'] = $no;
    $row['name'] = $entity->toLink();
    $row['uuid'] = $entity->uuid->value;
    $row['type'] = $entity->type->value;
    $status = '';
    if (!$entity->status->value) {
      $status = '<i class="far fa-eye-slash"></i>';
    }
    $state = $entity->state->value;
    $row['state'] = [
      'data' => [
        '#markup' => "$status $state",
      ],
    ];
    $row['uid']['data'] = [
      '#theme' => 'username',
      '#account' => $entity->getOwner(),
    ];
    $row['created'] = $this->dateFormatter->format($entity->getCreatedTime());
    return $row;
  }

  /**
   * Query.
   */
  private function query($registry_id) {
    $entities = [];
    $query = $this->storage->getQuery()
      ->condition('status', 1)
      ->condition('registry', $registry_id)
      ->sort('created', 'ASC')
      ->range(0, 100);
    $ids = $query->execute();
    if (!empty($ids)) {
      foreach ($this->storage->loadMultiple($ids) as $id => $entity) {
        $entities[$id] = $entity;
      }
    }
    return $entities;
  }

}
